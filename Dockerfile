FROM ruby:alpine

RUN wget -qO- https://github.com/just-containers/s6-overlay/releases/download/v1.21.7.0/s6-overlay-amd64.tar.gz|tar zx

RUN gem install google-api-client -v '~> 0.8'

RUN apk --no-cache add bash gawk curl grep recode jq coreutils openssl

ENV DST /data
ENV APP /app
COPY downloader ${APP}
COPY gmail/watch.rb /etc/periodic/daily/watch.rb
COPY gmail/run /etc/services.d/cron/run

ENV email ""
ENV password ""
ENV school ""

ENTRYPOINT ["/init"]
CMD ["/app/run"]

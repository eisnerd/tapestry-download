#!/bin/bash

toxml() {
#	cat
	iconv -f utf-8 -t utf-16|recode -qf u6..xml
}

sudo chown -R eisd:eisd .
grep -o 'Authored by[^<]*' */obs*|sed s@/.*added@@|while read d t; do touch "$d" -d "$t"; done

shopt -s nullglob

last_date=

for d in $(ls -1dt */); do
date="$(date +%F -d "$(grep -o 'Authored by[^<]*' $d/observation.html|sed 's@.*added@@')")"
if [ "$date" != "$last_date" ]; then
	f=day.$date.html
	[ -n "$last_date" ] && echo '<a class="next" href="'$f'">Next</a>'
	exec >&-
	exec > $f
	echo '<div class="row date"><div class="col"><h4>'"$(date "+%A, %_d %b %Y" -d "$date")</h4></div></div>"
	last_date="$date"
fi
title="$(sed -n '/<title>/,/<\/title>/s/<[^>]*>//p' $d/observation.html|toxml)"
notes="$(sed -n '/Notes/,/<\/p>/p' $d/observation.html|sed -n '/<p>/,$p'|toxml|sed 's/&lt;/</g;s/&gt;/>/g')"
for j in $d/*.jpg; do
cat <<END
  <div class="row">
    <div class="col s12 m7">
      <div class="card">
	<div class="card-image">
	  <img class="materialboxed" data-caption="$title" src="$j">
	  <span class="card-title">$title</span>
	</div>
	<div class="card-content">
          <span class="activator"><i data-title="$title" data-text="$notes" data-url="https://eisd.uk.to/tapestry/$j" class="material-icons right share">share</i></span>
          $notes<br/>$(grep -o 'Authored by[^<]*' $d/observation.html)
	</div>
      </div>
    </div>
  </div>
END
done
for m in $d/*.m4v; do
cat <<END
  <div class="row">
    <div class="col s12 m7">
      <div class="card">
        <div class="card-image">
          <video controls class="responsive-video" src="$m">
        </div>
        <div class="card-content">
          <h5>$title</h5>
          $notes
        </div>
      </div>
    </div>
  </div>
END
done
done

exec > index.html
cat <<END
  <!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <style>
        .card-title {
          text-shadow: 0.05em 0.05em 0.2em black;
        }
      </style>
    </head>

    <body>
      <div class="scroll"></div>
      <!--JavaScript at end of body for optimized loading-->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
      <script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/moment-precise-range-plugin@1.3.0/moment-precise-range.min.js"></script>
      <script>
        var pages = [
          $(ls -1rvQ day.*.html|paste -sd,)
        ];
	new InfiniteScroll(".scroll", {
          path: function() {
            return pages[this.loadCount];
          },
          append: ".row",
          history: false,
          prefill: true
        }).on( 'append', function(response, path, items) {
          items.forEach(function(x) {
            if (x.classList.contains("date")) {
              var y = x.children[0].children[0];
              var m = moment(y.innerText);
              var b = moment("2013-08-16");

              if (moment.duration(m.diff(moment.now())).days() != 0)
	        y.append(" (" + m.from(moment({hour:0})) + ")");
              x.children[0].innerHTML += "<i>Age " + moment.preciseDiff(b, m) + "</i>";
            }
          });
        });
        document.addEventListener('click', function (event) {
          if (!event.target.matches('.share')) return;

          if (navigator.share) {
            navigator.share({
              title: event.target.dataset.title,
              text: event.target.dataset.text,
              url: event.target.dataset.url,
            });
          }
          event.preventDefault();
        }, false);
        document.addEventListener('DOMContentLoaded', function() {
          var elems = document.querySelectorAll('.materialboxed');
          var instances = M.Materialbox.init(elems, {});
        });
      </script>
    </body>
  </html>
END
